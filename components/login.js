import React, { Component } from 'react';
import { AppRegistry, View, Text, TextInput, Button } from 'react-native';

//var Request = require('request');
//var jwt=require('jsonwebtoken');


var httpServer = "http://localhost:3000/";
var secretToken = "samplesecret";

onPressLoginButton = function(name, password){
	
	//var name = this.state.name;
	//var password = this.state.password;
	var url = httpServer + "api/v1/auth";

	//var token = jwt.sign({ name: name, password: password }, secretToken);

    /*
	Request.post({
    	"headers": { "content-type": "application/json" },
    	"url": url,
    	"body": JSON.stringify({
        	"token": token
    	})
	}, (error, response, body) => {
    	if(error) {
        	return console.dir(error);
    	}
    	console.dir(JSON.parse(body));
	});
	*/

	console.log('Evento de login. Name: ' + name + ' Password: ' + password);
};


class Login extends Component {
  
  constructor(props) {
    super(props);
    this.state = {name: '', password: ''};

  }	

  render() {

    return (
    	<View>
    		<Text>Name</Text>
      		<TextInput
        		editable = {true}
        		maxLength = {40}
        		onChangeText = {(text) => this.setState({name: text})}
      		/>
      		<Text>Password</Text>
      		<TextInput
        		editable = {true}
        		maxLength = {40}
        		onChangeText = {(text) => this.setState({password: text})}
      		/>
      		<Button
  			onPress={onPressLoginButton(this.state.name, this.state.password)}
  			title="Login"
  			accessibilityLabel="Learn more about this purple button"
			/>
      	</View>
    );
  }
}


export default class LoginApp extends Component {
  render() {
    return (
    	<Login/>
    );
  }
}

AppRegistry.registerComponent('eastereggpriceapp', () => LoginApp);